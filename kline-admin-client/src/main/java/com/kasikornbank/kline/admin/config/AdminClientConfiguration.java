package com.kasikornbank.kline.admin.config;

import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdminClientConfiguration {

    @Bean
    public Request.Options options() {
        return new Request.Options(30000, 30000);
    }
}
