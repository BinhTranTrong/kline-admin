package com.kasikornbank.kline.admin.interfaces;

import com.kasikornbank.kline.admin.SystemKbtgDto;
import com.kasikornbank.kline.admin.config.AdminClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "admin", configuration = AdminClientConfiguration.class)
public interface AdminClient {

    @GetMapping(value = "/system")
    List<SystemKbtgDto> getAll();

    @PostMapping(value = "/system", consumes = MediaType.APPLICATION_JSON_VALUE)
    SystemKbtgDto addSystemKbtg(@RequestBody SystemKbtgDto systemKbtgDto) throws Exception;
}