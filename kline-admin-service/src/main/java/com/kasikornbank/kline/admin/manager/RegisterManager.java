package com.kasikornbank.kline.admin.manager;

import com.kasikornbank.kline.common.base.BaseManager;

public class RegisterManager extends BaseManager<String, String> {

    @Override
    protected String getModule() {
        return "Binh Module";
    }

    @Override
    protected String getOperation() {
        return "Binh Operation";
    }

    @Override
    protected String onProcess(String name) throws Exception {
        return "Binh handsome";
    }
}
