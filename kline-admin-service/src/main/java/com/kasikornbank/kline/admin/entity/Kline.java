package com.kasikornbank.kline.admin.entity;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Document(collection = "klines")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Kline {

    @Id
    public ObjectId _id;

    @Field(value = "Name")
    private String name;

    @Field(value = "Created")
    private Date created;
}
