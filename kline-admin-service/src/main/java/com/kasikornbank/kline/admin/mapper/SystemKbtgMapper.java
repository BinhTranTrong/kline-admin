package com.kasikornbank.kline.admin.mapper;

import com.kasikornbank.kline.admin.SystemKbtgDto;
import com.kasikornbank.kline.admin.entity.SystemKbtg;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SystemKbtgMapper {

    // convert SystemKbtgDto to SystemKbtg
    SystemKbtgDto systemKbtgToSystemKbtgDto(SystemKbtg systemKbtg);

    // convert SystemKbtg to SystemKbtgDto
    SystemKbtg systemKbtgDtoToSystemKbtg(SystemKbtgDto systemKbtgDto);

    // convert SystemKbtg List to SystemKbtgDto List
    List<SystemKbtgDto> systemKbtgsToSystemKbtgDtos(List<SystemKbtg> systemKbtgs);
}
