package com.kasikornbank.kline.admin.service.implement;

import com.kasikornbank.kline.admin.KlineDto;
import com.kasikornbank.kline.admin.entity.Kline;
import com.kasikornbank.kline.admin.mapper.KlineMapper;
import com.kasikornbank.kline.admin.repository.KlineRepository;
import com.kasikornbank.kline.admin.service.KlineService;
import javassist.NotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class KlineServiceImpl implements KlineService {

    @Autowired
    private KlineRepository klineRepository;

    @Autowired
    private KlineMapper klineMapper;

    @Override
    public List<KlineDto> getAll() {
        return klineMapper.klinesToKlineDtos(klineRepository.findAll());
    }

    @Override
    public KlineDto findById (ObjectId objectId) throws NotFoundException {
        Kline kline = klineRepository.findBy_id(objectId);
        if(Objects.isNull(kline)) {
            throw new NotFoundException("Not found Kline");
        }
        return klineMapper.klineToKlineDto(kline);
    }

    @Override
    public KlineDto findByName(String name) throws NotFoundException {
        Kline kline = klineRepository.findByName(name);
        if(Objects.isNull(kline)) {
            throw new NotFoundException("Not found Kline");
        }
        return klineMapper.klineToKlineDto(kline);
    }

    @Override
    public KlineDto createKline(KlineDto klineDto) {
        klineDto.set_id(ObjectId.get());
        klineDto.setCreated(new Date());
        Kline kline = klineRepository.save(klineMapper.klineDtoToKline(klineDto));
        return klineMapper.klineToKlineDto(kline);
    }

    @Override
    public KlineDto updateKlineById(ObjectId objectId, KlineDto klineDto) throws Exception {
        Kline klineUpdate = klineMapper.klineDtoToKline(this.findById(objectId));
        klineUpdate.setName(klineDto.getName());
        return klineMapper.klineToKlineDto(klineRepository.save(klineUpdate));
    }

    @Override
    public void deleteKlineById(ObjectId objectId) throws NotFoundException {
        Kline klineDelete = klineMapper.klineDtoToKline(this.findById(objectId));
        klineRepository.delete(klineDelete);
    }
}
