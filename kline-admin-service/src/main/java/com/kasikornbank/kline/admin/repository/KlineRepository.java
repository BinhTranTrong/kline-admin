package com.kasikornbank.kline.admin.repository;

import com.kasikornbank.kline.admin.entity.Kline;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KlineRepository extends MongoRepository<Kline, String> {

    Kline findBy_id(ObjectId objectId);

    Kline findByName (String name);

}
