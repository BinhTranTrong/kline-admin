package com.kasikornbank.kline.admin.mapper;

import com.kasikornbank.kline.admin.KlineDto;
import com.kasikornbank.kline.admin.entity.Kline;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KlineMapper {

    // convert Kline to Kline Dto
    KlineDto klineToKlineDto (Kline kline);

    // convert Kline Dto to Kline
    Kline klineDtoToKline (KlineDto klineDto);

    // convert Kline List to Kline Dto List
    List<KlineDto> klinesToKlineDtos (List<Kline> klines);
}
