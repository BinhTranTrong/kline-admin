package com.kasikornbank.kline.admin.service.implement;

import com.kasikornbank.kline.admin.SystemKbtgDto;
import com.kasikornbank.kline.admin.entity.SystemKbtg;
import com.kasikornbank.kline.admin.mapper.SystemKbtgMapper;
import com.kasikornbank.kline.admin.repository.SystemKbtgRepository;
import com.kasikornbank.kline.admin.service.SystemKbtgService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class SystemKbtgServiceImpl implements SystemKbtgService {

    @Autowired
    private SystemKbtgRepository systemKbtgRepositoty;

    @Autowired
    private SystemKbtgMapper systemKbtgMapper;

    @Override
    public List<SystemKbtgDto> getAllSystemKbtgList() {
        List<SystemKbtg> systemKbtgList = systemKbtgRepositoty.findAll();
        if(CollectionUtils.isEmpty(systemKbtgList)) {
            return new ArrayList<>();
        }
        return systemKbtgMapper.systemKbtgsToSystemKbtgDtos(systemKbtgList);
    }

    @Override
    public SystemKbtgDto addSystemKbtg(SystemKbtgDto systemKbtgDto) throws Exception {
        if(Objects.isNull(systemKbtgDto)) {
            throw new Exception("SystemKbtg is null");
        }
        SystemKbtg systemKbtg = new SystemKbtg();
        systemKbtg.setName(systemKbtgDto.getName());
        systemKbtg.setCreated(new Date());
        return systemKbtgMapper.systemKbtgToSystemKbtgDto(systemKbtgRepositoty.save(systemKbtg));
    }
}
