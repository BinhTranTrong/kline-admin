package com.kasikornbank.kline.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Table(name = "system_kbtg")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SystemKbtg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    @NotBlank
    private String name;

    @Column(name = "created", nullable = false, updatable = false)
    private Date created;

}
