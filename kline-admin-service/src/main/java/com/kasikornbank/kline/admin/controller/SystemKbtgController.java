package com.kasikornbank.kline.admin.controller;

import com.kasikornbank.kline.admin.SystemKbtgDto;
import com.kasikornbank.kline.admin.service.SystemKbtgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SystemKbtgController {

    @Autowired
    private SystemKbtgService systemKbtgService;

    @GetMapping(value = "/system")
    public List<SystemKbtgDto> getAll() {
        return systemKbtgService.getAllSystemKbtgList();
    }

    @PostMapping(value = "/system")
    public SystemKbtgDto addSystemKbtg(@RequestBody SystemKbtgDto systemKbtgDto) throws Exception {
        return systemKbtgService.addSystemKbtg(systemKbtgDto);
    }

}
