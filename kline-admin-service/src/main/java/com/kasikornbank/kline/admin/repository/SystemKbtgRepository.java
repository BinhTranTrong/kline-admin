package com.kasikornbank.kline.admin.repository;

import com.kasikornbank.kline.admin.entity.SystemKbtg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemKbtgRepository extends JpaRepository<SystemKbtg, Long> {

}
