package com.kasikornbank.kline.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@SpringBootApplication
@EnableEurekaClient
public class KlineAdminServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlineAdminServiceApplication.class, args);
	}

}
