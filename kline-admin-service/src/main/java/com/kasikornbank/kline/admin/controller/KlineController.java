package com.kasikornbank.kline.admin.controller;

import com.kasikornbank.kline.admin.KlineDto;
import com.kasikornbank.kline.admin.manager.RegisterManager;
import com.kasikornbank.kline.admin.service.KlineService;
import com.kasikornbank.kline.common.base.BaseManager;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/kline")
public class KlineController {

    @Autowired
    private KlineService klineService;

    @GetMapping(value = "")
    public List<KlineDto> getAll() {
        return klineService.getAll();
    }

    @GetMapping(value = "/{id}")
    public KlineDto findById (@PathVariable("id") ObjectId id) throws NotFoundException {
        return klineService.findById(id);
    }

    @GetMapping(value = "/log")
    public ResponseEntity<?> logFindByName(HttpServletRequest header, @RequestParam("name") String name) throws Exception {
        return new RegisterManager().process(header, name);
    }

    @PostMapping(value = "")
    public KlineDto createKline (@RequestBody KlineDto klineDto) {
        return klineService.createKline(klineDto);
    }

    @PutMapping(value = "/{id}")
    public KlineDto updateKlineById (@PathVariable("id") ObjectId id,
                                     @RequestBody KlineDto klineDto) throws Exception {
        return klineService.updateKlineById(id, klineDto);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteKlineById (@PathVariable("id") ObjectId id) throws NotFoundException {
        klineService.deleteKlineById(id);
    }
}

