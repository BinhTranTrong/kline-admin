package com.kasikornbank.kline.admin.service;

import com.kasikornbank.kline.admin.SystemKbtgDto;

import java.util.List;

public interface SystemKbtgService {

    List<SystemKbtgDto> getAllSystemKbtgList();

    SystemKbtgDto addSystemKbtg(SystemKbtgDto systemKbtgDto) throws Exception;

}
