package com.kasikornbank.kline.admin.service;

import com.kasikornbank.kline.admin.KlineDto;
import javassist.NotFoundException;
import org.bson.types.ObjectId;

import java.util.List;

public interface KlineService {

    List<KlineDto> getAll();

    KlineDto findById (ObjectId objectId) throws NotFoundException;

    KlineDto createKline (KlineDto klineDto);

    KlineDto updateKlineById (ObjectId objectId, KlineDto klineDto) throws Exception;

    void deleteKlineById (ObjectId objectId) throws NotFoundException;

    KlineDto findByName (String name) throws NotFoundException;
}
