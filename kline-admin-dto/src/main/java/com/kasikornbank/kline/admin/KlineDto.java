package com.kasikornbank.kline.admin;

import lombok.*;
import org.bson.types.ObjectId;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class KlineDto {

    public ObjectId _id;

    private String name;

    private Date created;
}
